<?php

/**
 * @file
 * Bartik's theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template normally located in the
 * modules/system directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $hide_site_name: TRUE if the site name has been toggled off on the theme
 *   settings page. If hidden, the "element-invisible" class is added to make
 *   the site name visually hidden, but still accessible.
 * - $hide_site_slogan: TRUE if the site slogan has been toggled off on the
 *   theme settings page. If hidden, the "element-invisible" class is added to
 *   make the site slogan visually hidden, but still accessible.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['header']: Items for the header region.
 * - $page['featured']: Items for the featured region.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['triptych_first']: Items for the first triptych.
 * - $page['triptych_middle']: Items for the middle triptych.
 * - $page['triptych_last']: Items for the last triptych.
 * - $page['footer_firstcolumn']: Items for the first footer column.
 * - $page['footer_secondcolumn']: Items for the second footer column.
 * - $page['footer_thirdcolumn']: Items for the third footer column.
 * - $page['footer_fourthcolumn']: Items for the fourth footer column.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see bartik_process_page()
 * @see html.tpl.php
 */
?>
<div id="wrapper">
	<div id="wrapper__inner">
		<div id="header-block" class="<?php print $secondary_menu ? 'with-secondary-menu': 'without-secondary-menu'; ?>">
			<div class="uk-container uk-container-center">
				<div id="top-menu-block" class="uk-clearfix">
					
					<div id="top-menu-block__left">
						<ul class="uk-navbar-nav">
							<li><a href="/content/o-nas">О нас</a></li>
							<li><a href="/blog/">Блог</a></li>
							<li><a href="/content/kontakty">Контакты</a></li>
							<li><a href="/content/garantiya">Гарантия</a></li>
							<li><a href="/content/dostavka-i-oplata">Доставка и оплата</a></li>
						</ul>
					</div>
					
					<?php if ($secondary_menu): ?>
						<div id="top-menu-block__secondary">
						<?php print theme('links__system_secondary_menu', array(
								'links' => $secondary_menu,
								'attributes' => array(
								'class' => array('uk-navbar-nav'),
							)
						)); ?>
						</div> <!-- /#secondary-menu -->
					<?php endif; ?>
				</div>
				
				<div id="header-block__inner" class="uk-flex uk-flex-middle uk-flex-space-between">
					<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo-elem">
						<img src="/themes/bartik/logo.svg" alt="<?php print t('Home'); ?>" />
					</a>
					
					<?php print render($page['header']); ?>
					
					<a href="#offcanvas-block" class="uk-button uk-button-primary show-offcanvas" data-uk-offcanvas><i class="uk-icon-bars"></i></a>

				</div>
		
				
				<?php if ($page['highlighted']): ?>
					<div id="main-menu-block">
						<?php print render($page['highlighted']); ?>
					</div>
				<?php endif; ?>
				
				
			</div>
		</div> <!-- /#header -->

		<?php if ($page['help']): ?>
			<div id="home-banner-block">
				<div class="uk-container uk-container-center uk-flex uk-flex-middle">
					<?php print render($page['help']); ?>
				</div>
			</div>
		<?php endif; ?>


		<?php if ($messages): ?>
		<div id="messages-block" class="uk-block-small">
			<div class="uk-container uk-container-center">
				<?php print $messages; ?>
			</div>
		</div> <!-- /#messages -->
		<?php endif; ?>

		
		<?php if ($page['featured']): ?>
			<div id="featured-block">
				<div class="uk-container uk-container-center">
					<?php print render($page['featured']); ?>
				</div>
			</div> <!-- /#featured -->
		<?php endif; ?>
  
 

		<div id="main-wrapper-block" class="uk-block">
			<div id="main-wrapper-block__inner" class="uk-container uk-container-center">
				
				<?php if (!$is_front): ?>
					<?php if ($breadcrumb): ?>
						<div id="breadcrumb-block"><?php print $breadcrumb; ?></div>
					<?php endif; ?>
				<?php endif; ?>
				
				
				<div class="main-wrapper__content-inner uk-grid" data-uk-grid-margin>
				
				<?php if ($page['sidebar_first']): ?>
					<div id="sidebar-first-block">
						<?php print render($page['sidebar_first']); ?>
					</div> <!-- /#sidebar-first -->
				<?php endif; ?>
		
				
				<div id="main-wrapper__content" class="uk-width-1-1 <?php if ($page['sidebar_second']): ?>uk-width-medium-7-10<?php endif; ?>">
				
					<a id="main-content-link"></a>
					
					<?php if ($tabs): ?>
						<div class="tabs-block">
							<?php print render($tabs); ?>
						</div>
					<?php endif; ?>
					
					<?php print render($title_prefix); ?>
				
					<?php if ($title && $variables['node']->type != 'blog'): ?>
						<h1 class="title-block" id="page-title">
							<?php print $title; ?>
						</h1>
					<?php endif; ?>
					
					<?php print render($title_suffix); ?>
					
					
				
					<?php if ($action_links): ?>
						<ul class="action-links">
							<?php print render($action_links); ?>
						</ul>
					<?php endif; ?>
					
					<?php print render($page['content']); ?>
					
					<?php print $feed_icons; ?>
					
					<?php if ($page['productinfotable']): ?>
					<div id="product-info-table-block">
						<?php print render($page['productinfotable']); ?>
					</div>
					<?php endif; ?>
					
					<?php if ($page['sales']): ?>
					<div id="sales-block">
						<?php print render($page['sales']); ?>
					</div>
					<?php endif; ?>
				
				</div> <!-- /#content -->

				<?php if ($page['sidebar_second']): ?>
					<div id="sidebar-second" class="uk-width-medium-3-10 sidebar">
						<?php print render($page['sidebar_second']); ?>
					</div> <!-- /#sidebar-second -->
				<?php endif; ?>
				</div>

			</div> <!-- /#main -->
		</div> <!-- /#main-wrapper -->
		
		
		
		

		<?php if ($page['newsletter']): ?>
		<div id="newsletter-block">
			<div class="uk-container uk-container-center">
				<?php print render($page['newsletter']); ?>
			</div>
		</div>
		<?php endif; ?>
		
		<?php if ($page['tabs']): ?>
		<div id="tabs-cont">
			<div class="uk-container uk-container-center">
				<?php print render($page['tabs']); ?>
			</div>
		</div>
		<?php endif; ?>
		
		<?php if ($page['brands']): ?>
		<div id="brands-block" class="uk-block-small uk-block-muted">
			<div class="uk-container uk-container-center">
				<?php print render($page['brands']); ?>
			</div>
		</div>
		<?php endif; ?>
		
		<?php if ($page['about']): ?>
		<div id="about-block" class="uk-block">
			<div class="uk-container uk-container-center">
				<?php print render($page['about']); ?>
				<div class="about-block__more-button-cont"><button class="uk-button more-button">Подробнее</button></div>
			</div>
		</div>
		<?php endif; ?>
  
  
  
		<?php if ($page['footer']): ?>
		<div id="footer-block" class="uk-block uk-block-muted">
			<div class="uk-container uk-container-center">
				<?php print render($page['footer']); ?>
				
				<?php if ($page['copyright']): ?>
					<?php print render($page['copyright']); ?>
				<?php endif; ?>
			</div>
		</div>
		<?php endif; ?>

	</div>
</div> <!-- /#wrapper, /#wrapper__inner -->


<div id="offcanvas-block" class="uk-offcanvas">
    <div class="uk-offcanvas-bar uk-offcanvas-bar-flip">
	    
	    
	    
    </div>
</div>
