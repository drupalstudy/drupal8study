<?php

/*
 *@file
 *Contains \Drupal\d8study\Form\D8Form.
 */

namespace Drupal\d8study\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CssCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\StatusMessages;
use Drupal\node\Entity\Node;
use Drupal\file\Entity\File;

class D8Form extends FormBase
{

  public function getFormId()
  {
    return 'd8_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $form['message'] = array(
      '#type' => 'markup',
      '#markup' => $this->t('<strong><div class="ajax_form_message"></div></strong>'),
    );

    $form['user-name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Your name'),
      '#maxlength' => 30,
      '#required' => true,
      '#placeholder' => 'John Smith',
      '#ajax' => array(
        'callback' => '::validName',
        'effect' => 'fade',
        'event' => 'change',
        'progress' => array(
          'type' => 'throbber',
          'message' => NULL,
        ),
      ),
      '#suffix' => $this->t('<div class="name_valid"></div>'),
    );

    $form['user-email'] = array(
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#required' => true,
      '#placeholder' => 'email.address@example.com',
      '#ajax' => array(
        'callback' => '::validEmail',
        'event' => 'change',
        'progress' => array(
          'type' => 'throbber',
          'message' => NULL,
        ),
      ),
      '#suffix' => $this->t('<div class="email_valid"></div>'),
    );

    $form['user-phone'] = array(
      '#type' => 'tel',
      '#title' => $this->t('Phone number'),
      '#required' => true,
      '#placeholder' => '+380982095193',
      '#ajax' => array(
        'callback' => '::validPhone',
        'effect' => 'fade',
        'event' => 'change',
        'progress' => array(
          'type' => 'throbber',
          'message' => NULL,
        ),
      ),
      '#suffix' => $this->t('<div class="phone_valid"></div>'),
    );

    $form['text-box-field'] = array(
      '#type' => 'text_format',
      '#title' => $this->t('Message'),
      '#format' => 'full_html',
    );

    $form['field-managedfile'] = array(
      '#type' => 'managed_file',
      '#title' => t('File'),
      '#description' => $this->t('File formats:') . ' txt, pdf, xls',
      '#required' => true,
      '#suffix' => $this->t('<div class="file_exist"></div>'),
      '#upload_validators' => array(
        'file_validate_extensions' => array('txt pdf xls'),
      ),
    );

    $form['actions']['#type'] = 'actions';

    $form['actions']['button'] = array(
      '#type' => 'button',
      '#value' => $this->t('Create contact info'),
      '#button_type' => 'primary',
      '#ajax' => array(
        'callback' => '::submitForm',
        'event' => 'click',
        'progress' => array(
          'type' => 'throbber',
        ),
      ),
      '#suffix' => '<div class="validform"></div>',
    );

    $form['#prefix'] = $this->t('<div class="change_form">');

    $form['#suffix'] = $this->t('</div>');

    return $form;
  }

  /**
   * Name validation check.
   */
  public function validName(array &$form, FormStateInterface $form_state)
  {
    $response = new AjaxResponse();
    $name = $form_state->getValue('user-name');
    $text = '';

    if ($name == '') {
      $text = t('<p>Enter your name.</p>');
    }
    if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
      $text .= t('<p>Name must have only letters.</p>');
    }

    return $response->addCommand(new HtmlCommand('.name_valid',$text));
  }

  /**
   * Email validation check.
   */
  public function validEmail(array &$form, FormStateInterface $form_state)
  {
    $response = new AjaxResponse();
    $text = '';

    if(!filter_var($form_state->getValue('user-email'), FILTER_VALIDATE_EMAIL)) {
      $text = t('<p>Please, try again with typing email.</p>');
    }

    return $response->addCommand(new HtmlCommand('.email_valid', $text));

  }
  
  /**
   * Phone number validation check.
   */
  public function validPhone(array &$form, FormStateInterface $form_state)
  {
    $response = new AjaxResponse();
    $phone = $form_state->getValue('user-phone');
    $text = '';

    if ($phone == '') {
      $text .= t('<p>Enter phone number.</p>');
    }
    if (!preg_match("/^[\d\+]+$/",$phone)) {
      $text .= t('<p>Phone number should not have letters.</p>');
    }
    if ($phone[0] !== '+') {
      $text .= t('<p>Phone number should have + on beginning.</p>');
    }
    if (strlen($phone) < 12 and strlen($phone) > 0) {
      $text .= t('<p>Phone number to short.</p>');
    }

    return $response->addCommand(new HtmlCommand('.phone_valid', $text));
  }

  /**
   * Submit function.
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $respons = new AjaxResponse();
    $name = $form_state->getValue('user-name');
    $phone = $form_state->getValue('user-phone');
    $text = '';
    if ($name == '') {
      $text = t('Form is not valid.');
    }
    if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
      $text = t('Form is not valid.');
    }
    if ($phone == '') {
      $text = t('Form is not valid.');
    }
    if (!preg_match("/^[\d\+]+$/",$phone)) {
      $text = t('Form is not valid.');
    }
    if ($phone[0] !== '+') {
      $text = t('Form is not valid.');
    }
    if (strlen($phone) < 12 and strlen($phone) > 0) {
      $text = t('Form is not valid.');
    }
    if(!filter_var($form_state->getValue('user-email'), FILTER_VALIDATE_EMAIL)) {
      $text = t('Form is not valid.');
    }
    if ($text !== '') {
      return $respons->addCommand(new HtmlCommand('.validform', $text));
    }
    
    $form_file = $form_state->getValue('field-managedfile', 0);
    $file = File::load($form_file[0]);
    $file->setPermanent();
    $file->save();

    $node = Node::create([
      'type' => 'contact_info',
      'title' => $form_state->getValue('user-name'),
      'field_message' => $form_state->getValue('text-box-field'),
      'field_phone_number' => $form_state->getValue('user-phone'),
      'field_user_email' => $form_state->getValue('user-email'),
      'field_user_file' => $file,
    ])->save();

    return $respons->addCommand(new HtmlCommand('.change_form',t('Thanks for your submit!')));
  }

}