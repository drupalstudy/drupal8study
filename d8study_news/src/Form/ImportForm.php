<?php
namespace Drupal\d8study_news\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Render\FormattableMarkup;

class ImportForm extends ConfigFormBase 
{

  protected function getEditableConfigNames() 
  {
    return ['d8study_news.import_form'];
  }

  public function getFormId()
  {
  	return 'import_form';
  }

  public function buildForm( array $form, FormStateInterface $form_state)
  {
  	$queue = \Drupal::queue('rss_import_custom_module');
    if ($number_of_items = $queue->numberOfItems()) {
      $form['info_text'] = [
        '#type' => 'markup',
        '#markup' => new FormattableMarkup('<div>' . $this->t('This queue is already running, there are still:') . ' @number</div>', [
          '@number' => $number_of_items,
        ]),
      ];

      $form['delete'] = [
        '#type' => 'submit',
        '#value' => $this->t('Cancel current queue.'),
        '#disable' => TRUE,
      ];
    }
    else {
      $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Start Import.'),
        '#disable' => TRUE,
      ];
    }

    return $form;
  }

  public function validateForm( array &$form, FormStateInterface $form_state)
  {
  	parent::validateForm($form, $form_state);
  }

  public function submitForm( array &$form, FormStateInterface $form_state)
  {
    $xml = simplexml_load_file('https://news.finance.ua/ua/rss');
    $queue = \Drupal::queue('rss_import_custom_module');
    
    if ($form_state->getTriggeringElement()['#id'] == 'edit-delete') {
      $queue->deleteQueue();
    }
    else {
      $queue->createQueue();
      foreach ($xml->channel->item as $item) {
        $queue->createItem([
          'title' => (string) $item->title,
          'description' => (string) $item->description,
          'pubdate' => (string) $item->pubDate,
          'guid' => (string) $item->guid,
        ]);
      }
    }
  }
}